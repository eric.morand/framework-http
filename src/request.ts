import {parse} from "url";
import RouteParser from "route-parser";
import type {IncomingMessage} from "http";

export type HTTPMethod = 'CONNECT ' | 'DELETE' | 'GET' | 'HEAD' | 'OPTIONS ' | 'PATCH' | 'POST' | 'PUT' | 'TRACE';

export const extractParameters = <P extends Record<string, string>>(
    request: IncomingMessage,
    specification: string
): P | null => {
    const {pathname} = parse(request.url || '');

    if (pathname !== null) {
        const routeParser = new RouteParser<P>(specification);

        return routeParser.match(pathname) as P || null;
    }

    return null;
};