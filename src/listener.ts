import type {Listener} from "@arabesque/core";
import {createListener} from "@arabesque/listener-http";
import type {Context} from "./context";
import {createServer} from "http";
import {Readable} from "stream";

export const listener: Listener<number, Context> = (
    channel, handler
) => {
    const listener = createListener(
        createServer()
    );

    return listener(channel, (serverContext) => {
        const {message, response: serverResponse} = serverContext;

        return handler({
            request: message,
            response: {
                body: null,
                headers: serverResponse.getHeaders(),
                statusCode: serverResponse.statusCode,
                statusMessage: serverResponse.statusMessage
            },
            routeParameters: {}
        }).then((context) => {
            return new Promise((resolve) => {
                const {response} = context;
                const {body, headers, statusCode, statusMessage} = response;

                serverResponse.writeHead(statusCode, headers);
                serverResponse.statusMessage = statusMessage;

                const end = () => {
                    serverResponse.end(() => resolve(serverContext));
                }

                if (body === null) {
                    end();
                } else {
                    if (Readable.isReadable(body)) {
                        body.pipe(serverResponse);

                        serverResponse.on("close", end);
                    } else {
                        serverResponse.write(body, end);
                    }
                }
            });
        });
    });
};