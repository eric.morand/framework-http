import {Middleware} from "@arabesque/core";
import {Context} from "../context";
import {createANDMiddleware} from "@arabesque/logic-middlewares";

export const setContentType: (
    contentType: string
) => Middleware<Context> = (contentType) => (context, next) => {
    context.response.headers["Content-Type"] = contentType;

    return next(context);
};

export const setJpegContentType = setContentType("image/jpeg");

export const setContentTypeToJson = () => {
    return createANDMiddleware(
        (context, next) => {
            context.response.body = JSON.stringify(context.response.body);

            return next(context);
        },
        setContentType("application/json")
    );
}