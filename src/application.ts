import {createApplication as createCoreApplication, Middleware} from "@arabesque/core";
import {listener} from "./listener";
import type {Context} from "./context";

export const createApplication = (
    middleware: Middleware<Context>
) => {
    return (port: number) => {
        const application = createCoreApplication(listener, middleware);

        return application(port)
            .then(() => {
                console.info(`Application started, listening to TCP port ${port}`);
            });
    }
}